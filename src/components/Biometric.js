import React from 'react';
import {View, Text, Button, Alert, StyleSheet} from 'react-native';
import TouchID from 'react-native-touch-id';
import * as Keychain from 'react-native-keychain';

/*
1. check Biometric is there or not 1 =>yes ==>we show to add ,no ==>we not allow to touch id
2. if we have option to add touchid ==>1. Enter id and Password, and check enrolled is not if yes then we add to this to key chain
3. Login Process : 1. Click to login if enrolled then we show enrolled then match then directly to login with get keychain




*/

class Biometric extends React.Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      biometricType: '',
      login: false,
    };
  }

  componentDidMount() {
    /*check biometric is there or not if yes then which type of is it */
    this.checkBiometricStatus();
  }

  checkBiometricStatus = async () => {
    try {
      let res = await TouchID.isSupported();
      if (res) {
        this.setState({biometricType: res});
      }
    } catch (err) {
      Alert.alert('Biometric not supported');
    }
  };

  addCredentialWithBiometric = async () => {
    const {biometricType, username, password} = this.state;
    if (biometricType) {
      try {
        let res = await TouchID.authenticate('Authenticate with fingerprint'); //we can pass any message in place of 'Authenticate with fingerprint'
        if (res) {
          let username = 'sachingupta';
          let password = '123456';
          Keychain.setGenericPassword(username, password);
        }
      } catch (err) {
        Alert.alert('Something went wrong or not enrolled finger Print ');
      }
    } else {
      Alert.alert('Biometric not allowed in your device');
    }
  };

  accessKeychainCredential = async () => {
    const {biometricType} = this.state;
    if (biometricType) {
      let res = await TouchID.authenticate('Login with fingerPrint');
      if (res) {
        this.setState({login: true}, () => this.getCredentialHandler());
      }
    } else {
      Alert.alert('Biometric not allowed in your device');
    }
  };

  getCredentialHandler = async () => {
    const {login} = this.state;

    if (login) {
      let signature = await Keychain.getGenericPassword();
      if (signature) {
        const {username, password} = signature;
        this.setState({username: username, password: password}); //api hit here
      }
    } else {
      Alert.alert('Something went wrong');
    }
  };

  render() {
    const {biometricType, login, username, password} = this.state;
    return (
      <View style={styles.container}>
        {login ? (
          <View>
            <Text>Name:{username}</Text>
            <Text>Password:{password}</Text>
          </View>
        ) : (
          <View>
            <Button
              title={`Click for Add ${biometricType}`}
              onPress={() => this.addCredentialWithBiometric()}
            />
            <Button
              title={`Click for Login with ${biometricType}`}
              onPress={() => this.accessKeychainCredential()}
            />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
});
export default Biometric;
