import React from 'react';
import {View, Text} from 'react-native';
import Biometric from './src/components/Biometric';
const App = () => {
  return <Biometric />;
};

export default App;
